#!/usr/bin/env ruby

END { 
  # END block code 
  puts "END code block"
}
BEGIN { 
  # BEGIN block code 
  puts "BEGIN code block"
} 

puts "Hello, Ruby!";

print"\n"
print"\n"

my = 1
if my
  print"thats a stupid variable name \n"
end

print"\n"
print"\n"

ary = [1,2,3,4,5]
ary.each do |i|
   puts i
end

print"\n"
print"\n"

# blocks or methods in ruby

def test(a1="Ruby", a2="Perl")
   puts "The programming language is #{a1}"
   puts "The programming language is #{a2}"
end
test "C", "C++"
test

#object oriented programming

class Sample
   def hello
      puts "Hello Ruby!"
   end
end

# Now using above class to create objects
object = Sample. new
object.hello

$x = 10
class Customer
   @@no_of_customers=0
   def initialize(id, name)
      @cust_id=id
      @cust_name=name
   end
   def display_details()
      puts "Customer id #@cust_id"
      puts "Customer name #@cust_name"
      puts "global variable #$x"
   end
   def total_no_of_customers()
      @@no_of_customers += 1
      puts "Total number of customers: #@@no_of_customers"
   end
end

# Create Objects
cust1=Customer.new("1", "John")
cust2=Customer.new("2", "Poul")

# Call Methods
cust1.display_details()
cust1.total_no_of_customers()
cust2.display_details()
cust2.total_no_of_customers()

#modules in ruby and the mixin concept
module One
  def oneone
    puts "this the first module"
  end
end

module See
  def see1
    puts "this the second module"
  end
end

class Sample
  include One
  include See
end
s = Sample.new
s.oneone
s.see1
